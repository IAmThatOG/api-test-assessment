This a simple test assignment that will help us assess how you can read/write basic typescript API tests.
In this folder you can find [api-client](./api-client) folder. In this folder you can find an `api.ts` file which represents a generated API Client for [petstore swagger](https://petstore.swagger.io/#/) documentation.
It's a representation of a dummy API, that we would like you to test. This is API doesn't work, so you won't be able to execute your tests, but based on types you should be able to create some set of smoke tests. 

Expectations: 
1. Project should use Jest as a test runner
2. A set of smoke tests for 2-3 endpoints should be written ( you chooce what you want to test and how)
3. Project should compile without any errors. 
4. You can write your project structure in this folder and then just use `api-client` as a dependency in your tests and code
5. Short description of your steps/motivations and explanation of why you implemented a framework this way would be a great plus. 

Happy coding :) 

# Assessment Solution Steps

The approach taken for this solution was to create a folder which hold the tests
1. Create a folder in the root project folder
2. Initialise npm in that folder
3. Install required dependencies, one of which is Axios in order to gain access to Axios types
4. Setup jest and tsconfig
5. Write tests

Solution can be found [here](https://gitlab.com/IAmThatOG/api-test-assessment/-/blob/master/api-client-test/tests/api.spec.ts)