import { PetApiFactory, PetStatusEnum } from "../../api-client/api";
import { BASE_PATH } from "../../api-client/base";
import { PetApi, Configuration, Pet } from "../../api-client/index";
import { AxiosPromise, AxiosResponse } from "axios";

describe("Pet endpoint tests", () => {
    let samplePet: Pet;
    beforeEach(() => {
        samplePet = {
            id: 1,
            name: "doggie",
            category: {
                id: 1,
                name: "canine"
            },
            photoUrls: [],
            tags: [
                {
                    id: 1,
                    name: "canine pet"
                }
            ],
            status: PetStatusEnum.Available
        };
    });

    test("create pet returns success", async () => {
        //we want to use the default configuration, base path, and axios that's declared in BaseApi constructor
        var petApi = new PetApi();
        //calling addPet() returns a Promise<AxiosResponse<void>> so we need to await
        let response: AxiosResponse<void> = await petApi.addPet(samplePet);
        expect(response.data).toBe(200); //for a success response, the status should be 200
    });

    test("find pet by id returns success", async () => {
        //we want to use the default configuration, base path, and axios that's declared in BaseApi constructor
        var petApi = new PetApi();
        let response: AxiosResponse<Pet> = await petApi.getPetById(1); // we pass one as the id since that's the id of sample pet created
        expect(response.data).toBe(200); //for a success response, the status should be 200
        expect(response.data).not.toBeUndefined();//for a success response, it is perceived that the response cannot be undefined or null since pet with id 1 has been created in previous test
        expect(response.data).not.toBeNull();
        expect(response.data).toBe(samplePet);
    });

    test("delete pet returns success", async () => {
        //we want to use the default configuration, base path, and axios that's declared in BaseApi constructor
        var petApi = new PetApi();
        //calling addPet() returns a Promise<AxiosResponse<void>> so we need to await
        let response: AxiosResponse<void> = await petApi.deletePet(1, "special-key");
        expect(response.data).toBe(200); //for a success response, the status should be 200
    });
})